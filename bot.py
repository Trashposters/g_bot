import os
import discord
import dotenv
import json
import re
import random as rand
from datetime import datetime

rand.seed(datetime.now())
authorised_users=[]
pingables=[]
pingable_IDs=[]
jokes=[]
ignored=[]

commands=[
    {'invoke':'pinger',
    'args':[2],
    'auth':True,
    'desc':'Adds a pinger that pings the second user/role when a message from the first user/role is recieved'},
    {'invoke':'rmpinger',
    'args':[2],
    'auth':True,
    'desc':'Removes a pinger characterized by the 2 pinged users/roles'},
    {
    'invoke':'joke',
    'args':[0],
    'auth':False,
    'desc':'Prints a random boomer joke'
    },
    {
    'invoke':'help',
    'args':[0],
    'auth':False,
    'desc':'Prints some info about the available commands'
    },
    {
    'invoke':'square',
    'args':[1,2],
    'auth':False,
    'desc':'Prints a square. Replaces \'-\' with a space'
    },
    {
    'invoke':'clear',
    'args':[0],
    'auth':False,
    'desc':'Clears messages from bot within the last 50 messages'
    },
    {
    'invoke':'ignore',
    'args':[1],
    'auth':True,
    'desc':'Makes the bot ignore a user'
    },
    {
    'invoke':'unignore',
    'args':[1],
    'auth':True,
    'desc':'Stop ignoring an ignored user'
    }
    ]

def idFromP(s):
    return int(s[3:-1])

with open("./data/data.json",'r+') as f:
    file_dict=json.loads(f.read())
    f.close()
    authorised_users=file_dict['authorised_users']
    pingables=file_dict['pingables']
    ignored=file_dict['ignored']
with open('./data/jokin-16.txt') as f:
    jokes=f.read().split('}')
    f.close()

for pingable in pingables:
    pingable_IDs.append(pingable['id1'])
dotenv.load_dotenv()

TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()

@client.event
async def on_message(msg):
    if(len(msg.content)==0):
        return
    if(msg.author.id!=client.user.id):
        if(msg.content[0] == '!'):
            await handle_command(msg)
    await checkPingables(msg)
    if (re.search("scaly bf",msg.content) or re.search("scalie bf",msg.content)):
        await msg.add_reaction('🦎')

async def reply(msg,s):
    await msg.channel.send(s)

#TODO: Add ignore and unignore
#TODO: Scrape cat images and add command for fetching a random one

async def handle_command(msg):
    command=msg.content[1:].split(" ")
    command[0]=command[0].lower()
    if(await verifyCommand(command,msg.channel,msg.author)):
#===================================================================================================
        if(command[0]=='pinger'):
            if(not isPingable(command[1]) or not isPingable(command[2])):
                await msg.channel.send("Need 2 pingable arguments")
                return
            pinger={'id1':idFromP(command[1]),'ping1':command[1],'ping2':command[2],'guild':msg.guild.id}
            pingables.append(pinger)
            pingable_IDs.append(pinger['id1'])
            updateData()
            await reply(msg,"Pinger added")
#===================================================================================================
        if(command[0]=='rmpinger'):
            if(not isPingable(command[1]) or not isPingable(command[2])):
                await msg.channel.send("Need 2 pingable arguments")
                return
            for pingable in pingables:
                if(command[1]==pingable['ping1'] and command[2]==pingable['ping2'] and msg.guild.id == pingable['guild']):
                    pingables.remove(pingable)
                    updateData()
                    await reply(msg,"Ping removed")
                    return
            else:
                await reply(msg,"No such ping found")
#===================================================================================================
        if(command[0]=='joke'):
            joke=rand.choice(jokes)
            await reply(msg,joke)
#===================================================================================================
        if(command[0]=='square'):
            if(not command[1].isnumeric()):
                await reply(msg,"Please provide a number")
                return
            elif(int(command[1])<=0):
                await reply(msg,"Number can't be that tiny")
                return
            elif(int(command[1])>=20):
                await reply(msg,"Number can't be that big")
                return
            if (len(command) == 3):
                char=command[2]
            else:
                char = '#'
            await sendSquare(msg.channel,char=char,n=int(command[1]))
#===================================================================================================
        if(command[0]=='clear'):
            async for message in msg.channel.history(limit=50):
                if(message.author.id==client.user.id):
                    await message.delete()
#===================================================================================================
        if(command[0]=='help'):
            replyStr='```'
            for cmd in commands:
                replyStr=replyStr+'\nInvoke:'+" "*(18-len('Invoke:'))+cmd['invoke']
                replyStr=replyStr+'\nArgs:'+" "*(18-len('Args:'))+str(cmd['args'])
                replyStr=replyStr+'\nDescription:'+" "*(18-len('Description:'))+cmd['desc']+"\n"
                replyStr=replyStr+'\nNeeds auth:'+" "*(18-len('Needs auth:'))+str(cmd['auth'])+'\n'
                replyStr=replyStr+"\n===================================================================\n"
            replyStr=replyStr+'```'
            await msg.author.send(replyStr)


async def verifyCommand(command,channel,user):
    for cmd in commands:
        if cmd['invoke']==command[0]:
            if(cmd['auth'] and user.id not in authorised_users):
                await channel.send("{} requires authorization, silly {}".format(command[0],user.name))
                return False
            if ((len(command)-1) not in cmd['args']):
                await channel.send("Expected {} arguments".format(cmd['args']))
                return False
            else:
                return True
    else:
        await channel.send('No command like {}'.format(command[0]))
        return False

async def checkPingables(msg):
    if(msg.author.id in pingable_IDs):
        for pingable in pingables:
            if(pingable['id1']==msg.author.id and msg.guild.id == pingable['guild']):
                await msg.reply(pingable['ping2'])

async def sendSquare(channel,char='#',n=10):
    char=char.replace("-", " ")
    outStr=(char)*n
    outStr=(outStr+'\n')*n
    try:
        await channel.send(outStr)
    except discord.errors.HTTPException:
        await channel.send("That's too much man")
        
def updateData():
    with open("./data/data.json",'w+') as f:
        f.write(json.dumps({'authorised_users':authorised_users,'pingables':pingables,'ignored':ignored}))
        f.close()

def isPingable(strID):
    try:
        if(strID[0:2]=='<@' and strID[-1]=='>' and len(strID)==22):
            return True
        else:
            return False
    except:
        return False


client.run(TOKEN)